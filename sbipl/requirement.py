import subprocess

requests = ["pip", "install", "requests"]
bs4 = ["pip", "install", "beautifulsoup4"]
robobrowser = ["pip", "install", "robobrowser"]
azcaptchaapi = ["pip", "install", "azcaptchaapi"]
Django = ["pip", "install", "Django"]



### install requests package
out = subprocess.check_output(requests, shell = True)
### install beautifulsoup4 package
out = subprocess.check_output(bs4, shell = True)
### install robobrowser package
out = subprocess.check_output(robobrowser, shell = True)
### install azcaptchaapi package
out = subprocess.check_output(azcaptchaapi, shell = True)
### install flask package
out = subprocess.check_output(Django, shell = True)

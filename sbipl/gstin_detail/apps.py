from django.apps import AppConfig


class GstinDetailConfig(AppConfig):
    name = 'gstin_detail'

from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import gstin_form
from .Taxlist import taxpayerData

# Create your views here.
def gst_form(request):
	if request.method=='GET':
		form=gstin_form(request.GET)
		if form.is_valid():
			gstin=form.cleaned_data['gstin']
			pan=form.cleaned_data['pan']
			name=form.cleaned_data['name']
			if gstin!='' or pan!='' or name!='':
				ret_tax=taxpayerData(gstin,pan,name)
				return JsonResponse(ret_tax, safe=False)


	form=gstin_form()
	#return HttpResponse('GSTIN Deatil Form')
	return render(request,'index.html',{'form':form})

from . import cin
from . import llp
from . import view_doc
from . import view_doc_cin

def get_current(cinFDetails,est_year):
	import datetime
	curr_date=datetime.date.today()
	curr_year=curr_date.year

	st_year=2006
	addyear=None
	year_list=[]
	if int(est_year)<st_year:
		addyear=st_year
	else:
		addyear=int(est_year)
	list_=[str(addyear+st_year) for st_year in range(0,curr_year-addyear+1)]
	return list_

def get_doc_cin(cinFDetails,years_):
	if cinFDetails!=None:
		companyName= cin.companyName_(cinFDetails)
		return view_doc_cin.viewDocuments(companyName,cinFDetails,years_)

def get_doc_llp(llpFDetails):
	if llpFDetails!=None:
		companyName= llp.companyName_(llpFDetails)
		return view_doc.viewDocuments(companyName)
'''
if __name__=='__main__':
	#cinFDetails=input("Company CIN:")
	#est_year=cinFDetails.split()
	#est_year=cinFDetails[8:12]
	#est_year="".join(est_year)

	llpFDetails=input("Company LLP:")
	viewDoc=get_doc_llp(llpFDetails)
	
	#years_=get_current(cinFDetails,est_year)
	
	
	#viewDoc=get_doc_cin(cinFDetails,years_)
	print(viewDoc)
'''
	
	
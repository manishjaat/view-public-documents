from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import cin_,llp_
from .MCA import main



# Create your views here.
def cin_form(request):
	
	if request.method=='GET':
		form=cin_(request.GET)
		if form.is_valid():
			cinFDetails=form.cleaned_data['cin']
			print(cinFDetails)
			
			if cinFDetails!='':
				
				est_year=cinFDetails.split()
				est_year=cinFDetails[8:12]
				est_year="".join(est_year)
				years_=main.get_current(cinFDetails,est_year)
				viewDoc=main.get_doc_cin(cinFDetails,years_)
				
				return JsonResponse(viewDoc, safe=False)
			


	form=cin_()
	#return HttpResponse('GSTIN Deatil Form')
	return render(request,'mca_form.html',{'form':form})

def llp_form(request):
	
	if request.method=='GET':
		form=llp_(request.GET)
		if form.is_valid():
			llp=form.cleaned_data['llp']
			if llp!='':
				viewDoc=main.get_doc_llp(llp)
				
				return JsonResponse(viewDoc, safe=False)
			'''
			if gstin!='' or pan!='' or name!='':
				ret_tax=taxpayerData(gstin,pan,name)
				return JsonResponse(ret_tax, safe=False)
			'''


	form=llp_()
	#return HttpResponse('GSTIN Deatil Form')
	return render(request,'mca_form.html',{'form':form})

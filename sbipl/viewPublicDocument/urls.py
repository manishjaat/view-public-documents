from django.urls import path
from . import views
urlpatterns = [
	path('cin',views.cin_form),
	path('llp',views.llp_form),
]
